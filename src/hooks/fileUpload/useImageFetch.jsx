import React, {useEffect, useState} from 'react';
import axiosInstance from "../../api/axiosInterceptor";

const useImageFetch = (url: '') => {
    const [response, setResponse] = useState();
    const [error, setError] = useState();
    const [loading, setLoading] = useState();
    console.log(url)
    useEffect(() => {
        fetchImageData();
    }, [url]);

    const fetchImageData = () => {
        setLoading(true);
        axiosInstance.get({url}, {responseType: "blob"})
            .then((res) => {
                console.log(res.data);
                setResponse(res.data);
            })
            .catch((error) => {
                setError(error);
            })
            .finally(() => {
                setLoading(false);
            });
    }
    return {response, error, loading};
};

export default useImageFetch;