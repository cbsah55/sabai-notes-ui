import React, {useEffect, useState} from 'react';
import axiosInstance from "../../api/axiosInterceptor";

const UseFileUpload = (file) => {
    const [response, setResponse] = useState(null);
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');


    const uploadData =()=>{
        if (file !== undefined) {
            setLoading(true)
            const config = {headers: {'Content-Type': 'multipart/form-data'}};
            let fd = new FormData();
            fd.append("file", file);
            axiosInstance.post("/secure/user/material-upload", fd, config)
                .then((res) => {
                    setResponse(res.data.data);
                    setSuccessMessage(res.data.message);
                })
                .catch((error) => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();
                    setLoading(false);
                    setError(resMessage);
                })
                .finally(() => {
                    setLoading(false);
                });
        }
    };

    useEffect(()=>{
        uploadData();
    },[file]);

    return {uploadResponse: response, error, loading, successMessage};
};

export default UseFileUpload;
