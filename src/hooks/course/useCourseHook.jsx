import axiosInterceptor from "../../api/axiosInterceptor";


export const useCourseUpdate = async () => {
    const {response} = await axiosInterceptor.put("/secure/mod/course");
    return response;
};

export const useCourseDelete = async ({id}) => {
    const {response} = await axiosInterceptor.delete(`/secure/mod/course?id=${id}`);
    return response;
};
