import React, {useEffect, useState} from 'react';
import axiosInstance from "../../api/axiosInterceptor";

const useMaterialFetch = (refetch) => {
    const [response, setResponse] = useState();
    const [error, setError] = useState();
    const [loading, setLoading] = useState();

    useEffect(() => {
        console.log(refetch)
        fetchMaterials();
    }, []);

    const fetchMaterials = () => {
        setLoading(true);
        axiosInstance.get("/secure/user/myMaterials")
            .then((res) => {
                console.log(res.data.data);
                setResponse(res.data.data);
            })
            .catch((error) => {
                setError(error);
            })
            .finally(() => {
                setLoading(false);
            });
    }

    return {response, error, loading};
};

export default useMaterialFetch;