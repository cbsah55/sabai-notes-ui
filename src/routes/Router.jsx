import {Redirect, Route, Switch, useLocation} from "react-router-dom";
import Login from "../component/auth/Login";
import Register from "../component/auth/Register";
import MainPage from "../Pages/home";
import React from "react";
import ProtectedRoute from "./ProtectedRoute";
import Profile from "../Pages/Profile";
import PresentationLayout from "../PresentationLayout";
import {motion} from 'framer-motion';
import SideBarRoutes from "./SideBarRoutes";
import SideBar from "../component/header/SideBar";
import {Box} from "@material-ui/core";
import HeaderStyles from "../component/styles/HeaderStyles";
import ResetPassword from "../component/auth/ResetPassword";
import MaterialDetailPage from "../Pages/material/MaterialDetailPage";

const Router = (props) => {
    const location = useLocation();
    const classes = HeaderStyles();

    const pageVariants = {
        initial: {
            opacity: 0,
            scale: 0.99
        },
        in: {
            opacity: 1,
            scale: 1
        },
        out: {
            opacity: 0,
            scale: 1.01
        }
    };

    const pageTransition = {
        type: 'tween',
        ease: 'anticipate',
        duration: 0.4
    };
    return (

        <Switch>
            <Redirect exact from="/" to="/index"/>
            <Route path={['/index',
                "/login",
                "/register",
                "/resetPassword",
                '/material',
            ]}>
                <PresentationLayout>
                    <Switch location={location} key={location.pathname}>
                        <motion.div
                            initial="initial"
                            animate="in"
                            exit="out"
                            variants={pageVariants}
                            transition={pageTransition}>
                            <Route path="/index" component={MainPage}/>
                            <Route exact path="/login" component={Login}/>
                            <Route exact path="/register" component={Register}/>
                            <Route exact path="/resetPassword" component={ResetPassword}/>
                            <Route exact path="/" component={MainPage}/>
                            <ProtectedRoute exact path="/profile" component={Profile}/>
                            <Route exact path="/material/:id/" component={MaterialDetailPage}/>
                        </motion.div>
                    </Switch>
                </PresentationLayout>
            </Route>


            <Route path={[
                '/upload',
                '/home',
                '/downloads',
                '/myFavourites',
                '/notes',
                '/questions',
                '/manage',
                '/addInfo',
                '/courses',
                '/subjectDetails',
                '/materialList',


            ]}>
                <SideBar/>
                <Box className={classes.sideBarRoutesWrapper}>
                    <SideBarRoutes/>
                </Box>

            </Route>
        </Switch>

    );
}
export default Router;