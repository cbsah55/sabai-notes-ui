import React from 'react';
import {Redirect, Route} from "react-router-dom";
import useAuthService from "../service/auth/useAuthService";

const ProtectedRoute = ({component: Component, ...rest}) => {
    const {isAuthenticated} = useAuthService();
    return (
        <Route {...rest} render={props =>
            isAuthenticated() ? (<Component {...rest} {...props}/>) :
                (<Redirect to={
                        {
                            pathname: "/",
                            state: {
                                from: props.location
                            }
                        }
                    }
                    />
                )
        }
        />
    );
};

export default ProtectedRoute;