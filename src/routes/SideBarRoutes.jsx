import React from 'react';
import ProtectedRoute from "./ProtectedRoute";
import Download from "../component/BodyComponents/sideBarBody/Download";
import MyFavourite from "../component/BodyComponents/sideBarBody/MyFavourite";
import Notes from "../component/BodyComponents/sideBarBody/Notes";
import Questions from "../component/BodyComponents/sideBarBody/Questions";
import {Switch, useLocation} from "react-router-dom";
import Upload from "../component/BodyComponents/upload/Upload";
import Home from "../component/BodyComponents/sideBarBody/Home";
import {motion} from 'framer-motion';
import MaterialManagePage from "../Pages/material/MaterialManagePage";
import AddInfo from "../component/BodyComponents/InfoTabs/AddInfo";
import Courses from "../component/BodyComponents/sideBarBody/Courses";
import SubjectDetails from "../Pages/courses/SubjectDetails";
import MaterialList from "../Pages/courses/MaterialList";

const SideBarRoutes = () => {
    const location = useLocation();

    const pageVariants = {
        initial: {
            opacity: 0,
            scale: 0.99
        },
        in: {
            opacity: 1,
            scale: 1
        },
        out: {
            opacity: 0,
            scale: 1.01
        }
    };
    const pageTransition = {
        type: 'tween',
        ease: 'anticipate',
        duration: 0.4
    };
    return (
        <Switch location={location} key={location.pathname}>
            <motion.div
                initial="initial"
                animate="in"
                exit="out"
                variants={pageVariants}
                transition={pageTransition}>
                <ProtectedRoute exact path="/upload" component={Upload}/>
                <ProtectedRoute exact path="/home" component={Home}/>
                <ProtectedRoute exact path="/downloads" component={Download}/>
                <ProtectedRoute exact path="/myFavourites" component={MyFavourite}/>
                <ProtectedRoute exact path="/notes" component={Notes}/>
                <ProtectedRoute exact path="/questions" component={Questions}/>
                <ProtectedRoute exact path="/manage/:id/" component={MaterialManagePage}/>
                <ProtectedRoute exact path="/addInfo" component={AddInfo}/>
                <ProtectedRoute exact path="/courses" component={Courses}/>
                <ProtectedRoute exact path="/subjectDetails/:id/" component={SubjectDetails}/>
                <ProtectedRoute exact path="/materialList" component={MaterialList}/>
            </motion.div>
        </Switch>
    );
};

export default SideBarRoutes;