import React from 'react';

export default function useAuthService () {
    const logout = () => {
        localStorage.removeItem("token");
    }

    const getToken = () => {
        return JSON.parse(localStorage.getItem('token'));
    }

    const isAuthenticated = () => {
        const token = JSON.parse(localStorage.getItem('token'));
        console.log(token)
        if (token !== undefined && token) {
            return true;
        } else {
            return false;
        }
    };
    return {
        isAuthenticated,
        getToken,
    }
};

