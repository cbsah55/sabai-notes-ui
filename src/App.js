import '@fontsource/roboto';
import React from 'react';
import Router from "./routes/Router";
import CustomAppBar from "./component/header/AppBar";
import "@material-tailwind/react/tailwind.css";
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/default-layout/lib/styles/index.css';
import StickyFooter from "./component/header/Footer";


const App = () => {

    return (
        <React.Fragment>
            <CustomAppBar/>
            <Router/>
            <StickyFooter/>
        </React.Fragment>
    );
}

export default App;
