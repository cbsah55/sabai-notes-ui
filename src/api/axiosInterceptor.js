import Axios from "axios";

const API_URL = process.env.REACT_APP_API_URL;

 const axiosInstance = Axios.create({
    baseURL: API_URL,
    timeout: 20000,
  });
  
  // //add token to all request
  // axiosInstance.interceptors.request.use(function (config) {
  //   const currentUser = JSON.parse(localStorage.getItem('user'));
  //   if(currentUser){
  //   const { token } = currentUser.jwt;
  //     config.headers["Authorization"] = "Bearer " + token;
  //   }else{
  //     config.headers["Authorization"] ="";
  //   }
  //   return config;
  // });

  const requestHandler = request => {
    // Token will be dynamic so we can use any app-specific way to always   
    // fetch the new token before making the call
    const token = JSON.parse(localStorage.getItem('token'));
     if(token){
    request.headers.Authorization = 'Bearer '+token;  
     }
    return request;
};
  
  const responseHandler = response => {
    if (response.status === 401) {
        window.location = '/login';
    }

    return response;
};

const errorHandler = error => {
    return Promise.reject(error);
};

// Step-3: Configure/make use of request & response interceptors from Axios
// Note: You can create one method say configureInterceptors, add below in that,
// export and call it in an init function of the application/page.
axiosInstance.interceptors.request.use(
  (request) => requestHandler(request),
  (error) => errorHandler(error)
);

axiosInstance.interceptors.response.use(
    (response) => responseHandler(response),
    (error) => errorHandler(error)
 );

  export default axiosInstance;