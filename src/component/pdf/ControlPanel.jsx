import React from 'react';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import {Box, makeStyles} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    panel: {
        display: "flex",
        justifyContent: "center",
        padding: theme.spacing(0, 2, 0, 2)
    }
}));
const ControlPanel = (props) => {
    const classes = useStyles();
    const {pageNumber, numPages} = props;
    return (
        <>
            <Box className={classes.panel}>
                <Box>
                    <NavigateBeforeIcon/>
                </Box>

                <Box>
                    <p>Page {pageNumber} of {numPages}</p>
                </Box>
                <Box>
                    <NavigateNextIcon/>
                </Box>
            </Box>

        </>
    );
};

export default ControlPanel;