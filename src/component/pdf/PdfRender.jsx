import React, {useState} from 'react';
import {pdfjs} from "react-pdf";
import {Viewer} from "@react-pdf-viewer/core";
import {defaultLayoutPlugin} from "@react-pdf-viewer/default-layout";
import * as ReactDOM from "react-dom";

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.min.js`;

const PdfRender = ({pdfUrl}) => {
    const defaultLayoutPluginInstance = defaultLayoutPlugin();
    const [shown, setShown] = useState(false);

    const modalBody = () => (
        <div
            style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                overflow: 'auto',

                /* Fixed position */
                position: 'fixed',
                top: 60,

                /* Take full size */
                height: '100%',
                width: '100%',

                /* Displayed on top of other elements */
                zIndex: 9999,
            }}
        >
            <div
                style={{
                    alignItems: 'center',
                    backgroundColor: '#000',
                    color: '#fff',
                    display: 'flex',
                }}
            >
                <div style={{marginRight: 'auto'}}>Read Mode</div>
                <button
                    style={{
                        backgroundColor: '#357edd',
                        border: 'none',
                        borderRadius: '4px',
                        color: '#ffffff',
                        cursor: 'pointer',
                        padding: '8px',
                    }}
                    onClick={() => setShown(false)}
                >
                    Close
                </button>
            </div>
            <div
                style={{
                    flexGrow: 1,
                    overflow: 'auto',
                }}
            >
                {pdfUrl && <Viewer fileUrl={pdfUrl} plugins={[defaultLayoutPluginInstance]}/>}
            </div>
        </div>
    );

    return (
        <>
            <button
                style={{
                    backgroundColor: '#00449e',
                    border: 'none',
                    borderRadius: '.25rem',
                    color: '#fff',
                    cursor: 'pointer',
                    padding: '.5rem',
                    position: "relative",
                    zIndex: 1
                }}
                onClick={() => setShown(true)}
            >
                Click here to Read the pdf
            </button>
            {shown && ReactDOM.createPortal(modalBody(), document.body)}
        </>
    );
};

export default PdfRender;