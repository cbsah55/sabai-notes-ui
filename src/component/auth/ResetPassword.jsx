import React, {useRef, useState} from 'react';
import {
    Avatar,
    Box,
    Button,
    Container,
    CssBaseline,
    Grid,
    Link,
    makeStyles,
    Snackbar,
    TextField,
    Typography
} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import CircularProgress from "@material-ui/core/CircularProgress";
import {Alert} from "@material-ui/lab";
import {Link as RouterLink, useHistory} from "react-router-dom";
import {Copyright} from "@material-ui/icons";
import axiosInstance from "../../api/axiosInterceptor";
import {green} from "@material-ui/core/colors";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(3),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
}));
const ResetPassword = () => {
    const classes = useStyles();
    const form = useRef();
    const history = useHistory();
    const [token, setToken] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState("");
    const [snackBarOpen, setOpen] = useState(false);
    const [emailInput, setEmailInput] = useState(true);

    const handleEmailVerification = (e) => {
        e.preventDefault();
        setMessage("");
        setLoading(true);

        axiosInstance.post(`/public/passwordResetToken?email=${email}`
        ).then(setEmailInput(false))
            .catch(error => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();
                setOpen(true);
                setLoading(false);
                setEmailInput(true);
                setMessage(resMessage);
            }).finally(setLoading(false));
    };

    const handleReset = (e) => {
        e.preventDefault();
        setMessage("");
        setLoading(true);

        axiosInstance.post("/public/resetPassword", null, {
            params:
                {
                    token,
                    password
                }
        })
            .then((response) => {
                history.push("/login");
                window.location.reload();
            })
            .catch(error => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();
                setOpen(true);
                setLoading(false);
                setMessage(resMessage);
            });

    };

    const onChangeEmail = (e) => {
        const email = e.target.value;
        setEmail(email);
    };

    const onChangePassword = (e) => {
        const password = e.target.value;
        setPassword(password);
    };

    const onChangeToken = (e) => {
        const token = e.target.value;
        setToken(token);
    };
    const handleAlreadyHaveCode = () => {
        setEmailInput(false);
    };
    const handleRequestNewCode = () => {
        setEmailInput(true);
    };
    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Reset Password
                </Typography>
                <form className={classes.form} noValidate onSubmit={emailInput ? handleEmailVerification : handleReset}
                      ref={form}>
                    {emailInput ?
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Registered Email"
                            name="email"
                            autoComplete={email}
                            onChange={onChangeEmail}
                            autoFocus
                        />

                        :
                        <div>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="token"
                                label="Verification Code"
                                name="verificationToken"
                                onChange={onChangeToken}
                                autoFocus
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="newPassword"
                                label="New Password"
                                type="password"
                                id="newPassword"
                                onChange={onChangePassword}
                            />
                        </div>
                    }

                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        disabled={loading}
                        className={classes.submit}
                    >
                        {emailInput ? "Send Verification" : "Submit"}
                    </Button>
                    {loading && <CircularProgress size={24}/>}
                    {message.length > 0 && <Snackbar open={snackBarOpen} autoHideDuration={1000}
                                                     anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                                                     onClose={() => setOpen(false)}>
                        <Alert severity="error">
                            {message}
                        </Alert>
                    </Snackbar>}
                    <Grid container justifyContent={"space-between"}>
                        <Grid item>
                            <Link component={RouterLink} to="/login" variant="body-2">
                                {"Click here to login!"}
                            </Link>
                        </Grid>
                        <Grid item>
                            {!emailInput ?
                                <Link onClick={handleRequestNewCode} variant="body-2">
                                    {"Request new code."}
                                </Link>
                                :
                                <Link onClick={handleAlreadyHaveCode} variant="body-2">
                                    {"Already have a code?Click here."}
                                </Link>
                            }

                        </Grid>
                    </Grid>
                </form>
            </div>
            <Box mt={8}>
                <Copyright/>
            </Box>
        </Container>

    );
};

export default ResetPassword;