import {useRef, useState} from "react";
import {Link as RouterLink, useHistory} from 'react-router-dom'
import {Alert} from "@material-ui/lab"
import {
    Avatar,
    Box,
    Button,
    Checkbox,
    Container,
    CssBaseline,
    FormControlLabel,
    Grid,
    Link,
    makeStyles,
    Snackbar,
    TextField,
    Typography
} from "@material-ui/core";
import {green} from '@material-ui/core/colors';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {Copyright} from "@material-ui/icons";
import axiosInstance from "../../api/axiosInterceptor";
import CircularProgress from '@material-ui/core/CircularProgress';


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(3),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
      },
}));
const Login = () =>{
    const classes = useStyles();
    const form = useRef();
    const history = useHistory();
    const[email, setUsername] = useState("");
    const[password, setPassword] = useState("");
    const[loading, setLoading] = useState(false);
    const[message, setMessage] = useState("");
    const[snackBarOpen,setOpen]= useState(false);

    const onChangeUserName = (e) =>{
        const email = e.target.value;
        setUsername(email);
    };

    const onChangePassword = (e)=>{
        const password = e.target.value;
        setPassword(password);
    };

    const handleLogin=(e)=>{
        e.preventDefault();
        setMessage("");
        setLoading(true);

        axiosInstance.post("/public/authenticate",{
            email,
            password
        })
        .then((response) => {
            const {data} = response;
            localStorage.setItem("token", JSON.stringify(data.data.jwt));
            localStorage.setItem("name", JSON.stringify(data.data.name));
            history.push("/home");
            window.location.reload();
        })
        .catch(error=>{
            const resMessage =
              (error.response &&
                error.response.data &&
                error.response.data.message) ||
              error.message ||
              error.toString();
            setOpen(true);
            setLoading(false);
            setMessage(resMessage);
        });
    
    };



return(
    <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
            <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
                Log In
            </Typography>
            <form className={classes.form} noValidate onSubmit={handleLogin} ref={form}>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    onChange={onChangeUserName}
                    autoFocus
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    onChange={onChangePassword}
                />
                <FormControlLabel
                    control={<Checkbox value="remember" color="primary" />}
                    label="Remember me"
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled ={loading}
                    className={classes.submit}
                >
                    Log In
                </Button>
                {loading && <CircularProgress size={24}/>}
                {message.length >0 && <Snackbar open={snackBarOpen} autoHideDuration={1000} anchorOrigin={{vertical:'top',horizontal:'right'}} onClose={()=>setOpen(false)}>
        <Alert severity="error">
          {message}
        </Alert>
      </Snackbar>}
                <Grid container>
                    <Grid item xs>
                        <Link component={RouterLink} to="/resetPassword" variant="body-2">
                            {"Forgot password?"}
                        </Link>
                    </Grid>
                    <Grid item>
                        <Link component={RouterLink} to="/register" variant="body-2">
                            {"Don't have an account? Sign Up"}
                        </Link>
                    </Grid>
                </Grid>
            </form>
        </div>
        <Box mt={8}>
            <Copyright />
        </Box>
    </Container>
);

}

export default Login;
