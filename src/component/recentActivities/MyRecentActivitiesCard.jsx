import React from 'react';
import {Box, Card, makeStyles, Typography} from "@material-ui/core";
import DescriptionIcon from '@material-ui/icons/Description';
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    cardRoot:{
        elevation: 1,
        borderRadius: '20px',
        border: 1,
        display: "flex",
        height: 100,
        maxWidth: 450,
        alignItems: "center",
    },
    fontSmall:{
        fontSize: 12,
        fontWeight: 400,
        color: "#888888",
        lineHeight: 1.5,
    },
    fontLarge:{
        fontSize: "16px",
        fontWeight: 500,
        lineHeight: 1.2,
    },
    icon:{
        height: 50,
        width:60,
        color: "teal"
    }
}));
const MyRecentActivitiesCard = ({id,topic,imageIcon}) => {
    const classes = useStyles();

    return (
        <Link
            to={{
                pathname: `/material/${id}`,
                state: {id: id}
            }}>
            <Card className={classes.cardRoot}>
                <Box display={"flex"}>
                    <Box alignItems="center">
                        <DescriptionIcon className={classes.icon}/>
                    </Box>
                    <Box>
                        <Box>
                            <Typography variant={"body1"} className={classes.fontSmall}>Continue Reading</Typography>
                        </Box>
                        <Box marginRight={1}>
                            <Typography variant={"body1"} className={classes.fontLarge}>{topic}</Typography>
                        </Box>
                        <Typography variant={"body1"} className={classes.fontSmall}>You have read 8 pages</Typography>
                    </Box>
                </Box>

            </Card>
        </Link>
    );
};

export default MyRecentActivitiesCard;