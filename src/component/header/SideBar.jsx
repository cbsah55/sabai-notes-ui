import React, {useEffect, useState} from 'react';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import {Grid, Hidden, Typography} from "@material-ui/core";
import SideNavList from './SideNavList';
import useStyles from "../styles/HeaderStyles";
import useAuthService from "../../service/auth/useAuthService";
import MyAvatar from "../ui/MyAvatar";


const SideBar = ({mobileOpen, handleDrawerOpen}) => {
    const classes = useStyles();
    const {isAuthenticated} = useAuthService();
    const [name, setName] = useState("");


    useEffect(() => {
        isAuthenticated() && setName(JSON.parse(localStorage.getItem('name')));
    }, []);
    return (
        <div className={classes.sideNavRoot}>
            <Hidden smUp implementation={"css"}>
                <Drawer
                    variant="temporary"
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    ModalProps={{
                        keepMounted: true
                    }}
                    anchor={"left"}
                    open={mobileOpen}
                    onClose={handleDrawerOpen}>
                    <MyAvatar name={name}/>
                    <SideNavList/>
                </Drawer>
            </Hidden>
            <Hidden xsDown implementation={"css"}>
                <Drawer
                    variant="permanent"
                    classes={{
                        paper: classes.drawerPaper,
                    }}>
                    <Grid container direction={"row"} spacing={1} alignItems={"center"}>
                        <Grid item>
                            <MyAvatar name={name}/>
                        </Grid>
                        <Grid item>
                            <Typography variant={"h6"} noWrap={true}>{name}</Typography>
                        </Grid>
                    </Grid>
                    <Divider/>
                    <SideNavList/>
                </Drawer>
            </Hidden>
        </div>
    );
}

export default SideBar;