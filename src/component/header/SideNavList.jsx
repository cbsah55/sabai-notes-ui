import React from 'react';
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import FavoriteIcon from '@material-ui/icons/Favorite';
import HomeIcon from '@material-ui/icons/Home'
import PostAddIcon from '@material-ui/icons/PostAdd';
import {NavLink} from "react-router-dom";
import useStyles from "../styles/HeaderStyles";
import {Button} from "@material-ui/core";

const SideNavList = ({handleDrawerClose}) => {
    const classes = useStyles();
    const navListBasic = [
        {label: "Home", link: "/home", icon: <HomeIcon/>},
        {label: "My Favourites", link: "/myFavourites", icon: <FavoriteIcon/>},
        {label: "Notes", link: "/notes", icon: <MenuBookIcon/>},
        {label: "Upload", link: "/upload", icon: <CloudUploadIcon/>},
        {label: "Download", link: "/downloads", icon: <CloudDownloadIcon/>},
        {label: "Add Info", link: "/addInfo", icon: <PostAddIcon/>},
        {label: "Courses", link: "/courses", icon: <MenuBookIcon/>},
    ];


    return (
        <List>
            {navListBasic.map((item, i) => (
                <Button
                    size={"small"}
                    className={classes.navButton}
                    onClick={handleDrawerClose}
                    key={i}>
                <ListItem component={NavLink}
                          exact
                          to={item.link}
                          key={i}
                          className={classes.navLinks}
                          activeClassName={classes.activeNavLinks}>
                    <ListItemIcon>{item.icon}</ListItemIcon>
                    <ListItemText>{item.label}</ListItemText>
                </ListItem>
                </Button>
            ))}
        </List>
    );
};

export default SideNavList;