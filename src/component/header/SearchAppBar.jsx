import React, {useEffect, useState} from 'react';
import SearchIcon from "@material-ui/icons/Search";
import useStyles from "../styles/HeaderStyles";
import {InputBase} from "@material-ui/core";
import {Autocomplete} from "@material-ui/lab";
import axiosInterceptor from "../../api/axiosInterceptor";
import {useHistory} from "react-router-dom";

const SearchAppBar = (props) => {
    const classes = useStyles();
    let history = useHistory();
    const [open, setOpen] = React.useState(false);
    const [options, setOptions] = React.useState([]);
    const [searchParam, setSearchParam] = useState();


    const handleOnChange = (e) => {
        setSearchParam(e.target.value)
        setTimeout(() => {
            console.log("delayed");
        }, 2000);
        axiosInterceptor.get(`/public/${props.id}?filter=${searchParam}`)
            .then((res) => {
                const data = res.data.data;
                console.log(data)
                if (data !== undefined | null) {
                    setOptions(data);
                    // setOptions(Object.keys(data).map((key)=>data[key]));
                }
            })
            .catch(err => console.log(err));
    };
    useEffect(() => {
        if (!open) {
            setOptions([]);
        }
    }, [open]);

    const handleOptionChange = (value) => {
        value && history.push({
            pathname: `/material/${value.id}`,
            state: {id: value.id}
        });
    };
    return (
        <div className={classes.search}>
            <div className={classes.searchIcon}>
                <SearchIcon/>
            </div>
            <Autocomplete
                key={open}
                open={open}
                onOpen={() => {
                    setOpen(true);
                }}
                onClose={() => {
                    setOpen(false);
                }}
                getOptionSelected={(option, value) => option.name === value.name}
                getOptionLabel={(option) => option.name}
                options={options}
                onChange={(event, value) => handleOptionChange(value)}
                renderInput={(params) => (
                    <InputBase
                        fullWidth={true}
                        placeholder={"Search Notes"}
                        ref={params.InputProps.ref}
                        inputProps={params.inputProps}
                        onChange={handleOnChange}
                        autoFocus
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                    />
                )}
            />
        </div>
    );
};

export default SearchAppBar;