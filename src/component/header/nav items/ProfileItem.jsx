import React from 'react';
import {
    Avatar,
    Box, Button,
    IconButton,
    ListItem,
    ListItemIcon,
    ListItemText, makeStyles,
    Menu,
    MenuItem,
    withStyles
} from "@material-ui/core";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import SettingsIcon from "@material-ui/icons/Settings";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import logo from '../../../static/images/logo.svg';
import {deepPurple} from "@material-ui/core/colors";
import {useHistory} from "react-router-dom";

const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props) => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles((theme) => ({
    root: {
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                color: theme.palette.common.white,
            },
        },
    },
}))(MenuItem);

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    purple: {
        color: theme.palette.getContrastText(deepPurple[500]),
        backgroundColor: deepPurple[500],
    },
}));
function SendIcon(props: { fontSize: string }) {
    return null;
}

function DraftsIcon(props: { fontSize: string }) {
    return null;
}

const dropDownData =[
    {label :"Settings",icon: <SettingsIcon/>},
    {label :"Logout",icon: <ExitToAppIcon/>}
];
const ProfileItem = () => {
    const classes = useStyles();
    const history = useHistory();
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    const logOut = () => {
        localStorage.removeItem("token");
        history.push('/');
        window.location.reload();
    };

    const handleItemClick=(e)=> {
        console.log(e);
    }

    return (
        <Box>
            <Button
            aria-controls='simple-menu'
            aria-haspopup= 'true'
            onClick={handleClick}
            startIcon ={<Avatar className={classes.purple}>CS</Avatar>}
            >

            </Button>
            <StyledMenu
                id="customized-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {dropDownData.map((item,i)=>
                    <StyledMenuItem value={item.label} onClick={handleItemClick} key={i} >
                        <ListItemIcon >
                            {item.icon}
                        </ListItemIcon>
                        <ListItemText >{item.label}</ListItemText>
                    </StyledMenuItem>
                )};
            </StyledMenu>
        </Box>
    );
};

export default ProfileItem;