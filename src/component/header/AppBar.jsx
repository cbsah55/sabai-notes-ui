import useAuthService from '../../service/auth/useAuthService';
import {Link, useHistory} from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import logo from '../../static/images/logo.png';
import {Box, Hidden, IconButton} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import useStyles from "../styles/HeaderStyles";
import SearchAppBar from "./SearchAppBar";

const CustomAppBar = () => {
    const classes = useStyles();
    const history = useHistory();
    const {isAuthenticated} = useAuthService();


    const logOut = () => {
        console.log("logout button pressed");
        localStorage.removeItem("token");
        localStorage.removeItem("name");
        history.push('/');
        window.location.reload();
    };

    const routeLogin = () => {
        let path = '/login';
        history.push(path);
    };
    const routeRegister = () => {
        let path = '/register';
        history.push(path);
    }

    console.log(isAuthenticated());

    return (
        <AppBar position="fixed" className={classes.appBar}>
            <Toolbar className={classes.appToolbar}>

                <Link to={"/home"}>
                    <div>
                        <img src={logo} alt="logo" className={classes.appBarLogo}/>
                    </div>
                </Link>
                <SearchAppBar id={"materials-filter"}/>

                <div>
                    <Hidden smDown>
                        {isAuthenticated() ?
                            <Button variant="contained" color="primary" onClick={logOut}>Logout</Button>

                            /*<Box className={classes.appBarBox}>
                                <ProfileItem/>
                            </Box>*/ :
                            (
                                <Box className={classes.appBarBox}>
                                    <Button variant="contained" color="primary" onClick={routeLogin}>Login</Button>
                                    <Button variant="contained" color="primary"
                                            onClick={routeRegister}>Register</Button>
                                </Box>
                            )
                    }
                </Hidden>
                <Hidden mdUp>
                <IconButton color={"primary"}>
                    <MenuIcon/>
                </IconButton>
                </Hidden>
                </div>
            </Toolbar>
        </AppBar>
    );
};

export default CustomAppBar;