import React, {useEffect} from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const ErrorSnackBar = ({error}) => {
    const [state, setState] = React.useState({
        open: false,
        vertical: 'top',
        horizontal: 'right',
    });

    useEffect(() => {
        if (error !== null || undefined) {
            setState({...state, open: true});
        }
    }, []);

    const {vertical, horizontal, open} = state;

    const handleClose = () => {
        setState({...state, open: false});
    };

    return (
        <Snackbar open={open} autoHideDuration={6000} onClose={handleClose} anchorOrigin={{vertical, horizontal}}>
            <Alert onClose={handleClose} severity="error">
                {error}
            </Alert>
        </Snackbar>
    );
};

export default ErrorSnackBar;