import React from 'react';
import {Box, Card, CardActionArea, Grid, makeStyles, Typography} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    cardRoot:{
        elevation : 1,
        borderRadius: '20px',
        border : 1,
        minHeight : 92,
        position: "relative",
        width: '100%',
        maxWidth : "max-content",
        marginTop : 4,
    },
    fontSmall:{
        fontSize: 12,
        fontWeight: 400,
        color: "#888888",
        lineHeight: 1.5,
        margin: 3
    },
    fontLarge:{
        display: "-webkit-box",
        overflow: "hidden",
        fontSize: "16px",
        fontWeight: 500,
        lineHeight: 1.2,
        WebkitBoxOrient: "vertical",
        WebkitLineClamp: "2"
    },
    box:{
        height: 50,
        width:60,
        marginTop: 20,
        padding: 5,
        objectFit : "fill"
    },
}));
const MyRecentActivitiesCard = ({link,topic,thumb}) => {
    const classes = useStyles();
    return (
        <Card className={classes.cardRoot}>
            <CardActionArea href={link}>
                <Grid container >
                    <Grid item md={2} sm={2} lg={2} xs={2}>
                        <Box>
                            <img src={thumb}/>
                        </Box>
                    </Grid>
                    <Grid item md={10} sm={10} lg={10} xs={10}>
                        <Typography variant={"body1"} className={classes.fontSmall}>Continue Reading</Typography>
                        <Typography variant={"body1"} className={classes.fontLarge}>{topic}</Typography>
                        <Typography variant={"body1"} className={classes.fontSmall}>You have read 8 pages</Typography>
                    </Grid>

                </Grid>
            </CardActionArea>
        </Card>
    );
};

export default MyRecentActivitiesCard;