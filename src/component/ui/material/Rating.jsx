import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import axiosInstance from "../../../api/axiosInterceptor";
import ErrorSnackBar from "../../header/ErrorSnackBar";
import SuccessSnackBar from "../../header/SuccessSnackBar";

const labels = {
    0.5: 'Useless',
    1: 'Useless+',
    1.5: 'Poor',
    2: 'Poor+',
    2.5: 'Ok',
    3: 'Ok+',
    3.5: 'Good',
    4: 'Good+',
    4.5: 'Excellent',
    5: 'Excellent+',
};

const useStyles = makeStyles({
    root: {
        width: 200,
        display: 'flex',
        alignItems: 'center',
    },
});
const MyRating = (props) => {
    const [value, setValue] = React.useState(0);
    const [hover, setHover] = React.useState(-1);
    const classes = useStyles();
    const {currValue, id} = props;
    const [successMessage, setSuccesssMessage] = useState("");
    const [error, setError] = useState("");

    useEffect(() => {
        currValue && setValue(currValue);
    }, []);

    const handleRating = (event, newValue) => {
        setValue(newValue);
        axiosInstance.post("/public/material-rating", null, {
            params: {
                id,
                value: newValue
            }
        }).then(res => setSuccesssMessage(res.data.message)).catch(error => {
            const resMessage =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();
            setError(resMessage);
        });
    };

    return (
        <div className={classes.root}>
            <Rating
                name="hover-feedback"
                value={value}
                precision={0.5}
                onChange={(event, newValue) => handleRating(event, newValue)}
                onChangeActive={(event, newHover) => {
                    setHover(newHover);
                }}
            />
            <Box display={"flex"}>
                {value !== null && <Box ml={2}>{labels[hover !== -1 ? hover : value]}</Box>}
            </Box>
            {error.length > 0 && <ErrorSnackBar message={error}/>}
            {successMessage.length > 0 &&
            <SuccessSnackBar successMessage={successMessage}/>}
        </div>
    );
};

export default MyRating;