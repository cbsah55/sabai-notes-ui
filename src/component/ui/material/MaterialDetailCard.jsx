import React from 'react';
import {Box, Card, CardContent, CardHeader, makeStyles, Typography} from "@material-ui/core";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import CloseOutlinedIcon from "@material-ui/icons/CloseOutlined";

const useStyles = makeStyles((theme) => ({

    cardSide: {
        padding: "5px",
        maxWidth: 400,
        maxHeight: 400,
        height: "100%",
        width: "100%",
        margin: theme.spacing(0, 0, 3, 0)
    },
    cardHeader: {
        background: "#efefef",
        height: "30px",
        padding: "3px",
        fontSize: "medium",
    },
    cardContent: {
        overflow: "auto"
    }
}));
const MaterialDetailCard = ({response}) => {
    const classes = useStyles()

    return (
        <Card className={classes.cardSide} style={{width: "100%"}}>
            <CardHeader
                title={"Details"}
                className={classes.cardHeader}
            />
            <CardContent className={classes.cardContent}>
                <Box display={"flex"} justifyContent="space-between" marginBottom={2}>
                    <Box>
                        <Typography variant={"body1"}>Verified -</Typography>
                    </Box>
                    <Box>
                        <Typography variant={"body1"}>{response.verified ? <VerifiedUserIcon color={"green"}/> :
                            <CloseOutlinedIcon color={"#ff0000"}/>}</Typography>
                    </Box>
                </Box>
                <Box display={"flex"} justifyContent="space-between" marginBottom={2}>
                    <Box>
                        <Typography variant={"body1"}>No. of pages </Typography>
                    </Box>
                    <Box>
                        <Typography variant={"body1"}>{response?.pages}</Typography>
                    </Box>
                </Box>

                <Box display={"flex"} justifyContent="space-between" marginBottom={2}>
                    <Box>
                        <Typography variant={"body1"}>Type</Typography>
                    </Box>
                    <Box>
                        <Typography variant={"body1"}>{response?.type ? response.type : "-"}</Typography>
                    </Box>
                </Box>

                <Box display={"flex"} justifyContent="space-between" marginBottom={2}>
                    <Box>
                        <Typography variant={"body1"}>Subject</Typography>
                    </Box>
                    <Box>
                        <Typography
                            variant={"body1"}>{response?.subject ? response.subject.name : "-"}</Typography>
                    </Box>
                </Box>

                <Box display={"flex"} justifyContent="space-between" marginBottom={2}>
                    <Box>
                        <Typography variant={"body1"}>University</Typography>
                    </Box>
                    <Box>
                        <Typography
                            variant={"body1"}>{response?.institute ? response.institute.name : "-"}</Typography>
                    </Box>
                </Box>

                <Box display={"flex"} justifyContent="space-between" marginBottom={2}>
                    <Box>
                        <Typography variant={"body1"}>Uploaded date</Typography>
                    </Box>
                    <Box>
                        <Typography
                            variant={"body1"}>{response?.uploadedDate ? response.uploadedDate : "-"}</Typography>
                    </Box>
                </Box>

                <Box display={"flex"} justifyContent="space-between" marginBottom={2}>
                    <Box>
                        <Typography variant={"body1"}>Views</Typography>
                    </Box>
                    <Box>
                        <Typography variant={"body1"}>{response?.viewCount ? response.viewCount : "-"}</Typography>
                    </Box>
                </Box>
            </CardContent>
        </Card>
    );
};

export default MaterialDetailCard;