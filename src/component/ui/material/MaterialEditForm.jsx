import React, {useState} from 'react';
import {Box, Button, Card, Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import AutoCompleteSearch from "../search/AutoCompleteSearch";
import DropDown from "../search/DropDown";
import axiosInterceptor from "../../../api/axiosInterceptor";

const useStyles = makeStyles((theme) => ({

    basicDetails: {
        height: "100%",
        width: 400,
        padding: 6
    }

}));
const MaterialEditForm = (props) => {
    const classes = useStyles();
    const [subjectId, setSubjectId] = useState('');
    const [instituteId, setInstituteId] = useState('');
    const [facultyId, setFacultyId] = useState('');
    const [name, setName] = useState(props.materialDetail?.name);
    const [description, setDescription] = useState();
    const [academicSession, setAcademicSession] = useState();
    const [type, setType] = useState();
    const material = props.materialDetail;
    const [loading, setLoading] = useState(false);

    const handleClose = () => {
        props.onEditCancel(false);
    };


    const handleSubmit = () => {
        setLoading(true);
        axiosInterceptor.put("/secure/user/material-update", {
            id: material.id,
            name,
            description,
            subjectId,
            instituteId,
            academicSession,
            type,
        })
            .then(res => console.log(res))
            .catch(error => console.log(error))
            .finally(setLoading(false));
        props.onSuccess(loading);
    };

    const handleSubjectCallback = (id) => {
        setSubjectId(id);
    };

    const handleInstituteCallBack = (id) => {
        setInstituteId(id);
    };

    const handleFacultyCallback = (id) => {
        setFacultyId(id);
    };

    const handleNameChange = (e) => {
        setName(e.target.value);
    };

    const handleDesChange = (e) => {
        setDescription(e.target.value);
    };

    const handleAcademicSessionsCallback = (value) => {
        setAcademicSession(value);
    };
    const handleNotesTypeCallback = (value) => {
        setType(value);
    };
    return (
        <React.Fragment>
            <Card className={classes.basicDetails}>
                <Typography variant="h6" gutterBottom>
                    Edit Details:
                </Typography>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            id="name"
                            name="name"
                            label="Name"
                            fullWidth
                            value={name}
                            onChange={handleNameChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <DropDown id={"material-types"} uri={"mod"} label={"Type"}
                                  callBackValue={handleNotesTypeCallback}
                                  value={material?.type}/>
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <AutoCompleteSearch label={"Subject"} id={"subjects"} callBackValue={handleSubjectCallback}/>
                    </Grid>

                    <Grid item xs={12} sm={12}>
                        <AutoCompleteSearch label={"University"} id={"institutes"}
                                            callBackValue={handleInstituteCallBack}/>
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <AutoCompleteSearch label={"Faculty"} id={"faculties"} callBackValue={handleFacultyCallback}/>
                    </Grid>

                    <Grid item xs={12} sm={12}>
                        <DropDown id={"academic-sessions"} uri={"mod"} label={" Year/Semester"}
                                  callBackValue={handleAcademicSessionsCallback}/>
                    </Grid>

                    <Grid item xs={12} sm={12}>
                        <TextField
                            id="desc"
                            name="description"
                            label="Description"
                            onChange={handleDesChange}
                            fullWidth
                            autoComplete="Desc..."
                        />
                    </Grid>

                    <Grid item xs={12} sm={12}>
                        <Box display="flex" justifyContent={"space-between"}>
                            <Box>
                                <Button variant={"contained"} color={"secondary"} size={"small"}
                                        onClick={handleClose}>Cancel</Button>
                            </Box>
                            <Box>
                                <Button variant={"contained"} size={"small"} color={"primary"}
                                        onClick={handleSubmit}>Submit</Button>
                            </Box>
                        </Box>
                    </Grid>
                </Grid>
            </Card>
        </React.Fragment>
    );
};

export default MaterialEditForm;