import React, {useEffect, useState} from 'react';
import {
    Button,
    Card,
    CardContent,
    FormControlLabel,
    FormGroup,
    makeStyles,
    Switch,
    Typography
} from "@material-ui/core";
import axiosInterceptor from "../../../api/axiosInterceptor";
import ShowBackDrop from "../../header/ShowBackDrop";

const useStyles = makeStyles((theme) => ({

    basicDetails: {
        height: "100%",
        width: 400,
        padding: 6
    }

}));
const MaterialSwitches = (props) => {
    const classes = useStyles();
    const material = props.materialDetails;
    const [downloadable, setDownloadable] = useState(false);
    const [printable, setPrintable] = useState(false);
    const [handWritten, setHandWritten] = useState(false);
    const [isPublic, setIsPublic] = useState(false);
    const [loading, setLoading] = useState(false);


    useEffect(() => {

        material && setDownloadable(material?.downloadable);
        material && setHandWritten(material?.handWritten);
        material && setPrintable(material?.printable);
        material && setIsPublic(material?.isPublic);
    }, [material])

    const handleUpdate = () => {
        setLoading(true);
        axiosInterceptor.put("/secure/user/material-update", {
            id: material.id,
            name: material.name,
            downloadable,
            printable,
            handWritten,
            isPublic,
        })
            .then(res => console.log(res))
            .catch(error => console.log(error))
            .finally(setLoading(false));
        console.log(loading)
    };

    const handleDownloadableChange = (event) => {
        setDownloadable(event.target.checked);
    };

    const handlePrintableChange = (event) => {
        setPrintable(event.target.checked);
    };

    const handleHandWrittenChange = (event) => {
        setHandWritten(event.target.checked);
    };
    const handleIsPublicChange = (event) => {
        setIsPublic(event.target.checked);
    };
    return (
        <>
            <ShowBackDrop loading={loading}/>
            <Card className={classes.basicDetails}>
                <Typography variant={"body1"} gutterBottom>Other Details</Typography>
                <CardContent>
                    <FormGroup row>
                        <FormControlLabel
                            control={
                                <Switch
                                    checked={downloadable}
                                    onChange={handleDownloadableChange}
                                    color="primary"
                                />
                            }
                            label="Downloadable"
                        />

                        <FormControlLabel
                            control={
                                <Switch
                                    checked={printable}
                                    onChange={handlePrintableChange}
                                    color="primary"
                                />
                            }
                            label="Printable"
                        />

                        <FormControlLabel
                            control={
                                <Switch
                                    checked={handWritten}
                                    onChange={handleHandWrittenChange}
                                    color="primary"
                                />
                            }
                            label="Handwritten"
                        />
                        <FormControlLabel
                            control={
                                <Switch
                                    checked={isPublic}
                                    onChange={handleIsPublicChange}
                                    color="primary"
                                />
                            }
                            label="Is Public"
                        />
                        <Button variant={"contained"} color={"primary"} style={{marginTop: "25px"}}
                                onClick={handleUpdate}>Update Changes</Button>
                    </FormGroup>
                </CardContent>
            </Card>
        </>
    );
};

export default MaterialSwitches;