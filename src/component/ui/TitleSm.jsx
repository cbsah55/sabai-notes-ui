import {Box, Grid, makeStyles} from "@material-ui/core";
import React from 'react';

const useStyles = makeStyles((theme) => ({
    title: {
        color: "rgb(0, 0, 0)",
        display: "flex",
        fontSize: "20px",
        alignItems: "center",
        borderLeft: "4px solid rgb(16, 43, 69)",
        fontWeight: 600,
        borderTopColor: "rgb(16, 43, 69)",
        borderRightColor: "rgb(16, 43, 69)",
        borderBottomColor: "rgb(16, 43, 69)",
        paddingLeft: "12px",
        margin: theme.spacing(2, 0, 0, 0),
        paddingRight: "12px",
        justifyContent: "space-between"
    },
}));

const TitleSm = ({title}) => {
    const classes = useStyles();
    return (
        <Grid item>
        <Box display='block' padding={1}>
            <Box>
                <Box className={classes.title}>{title}</Box>
            </Box>
        </Box>
        </Grid>
    );
};

export default TitleSm;