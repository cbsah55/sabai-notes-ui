import React from 'react';
import {Avatar, makeStyles} from "@material-ui/core";
import {deepPurple} from "@material-ui/core/colors";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    purple: {
        color: theme.palette.getContrastText(deepPurple[500]),
        backgroundColor: deepPurple[500],
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
}));
const MyAvatar = (props) => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Avatar className={classes.purple}>{props.name.slice(0, 1)}</Avatar>
        </div>
    );
};

export default MyAvatar;