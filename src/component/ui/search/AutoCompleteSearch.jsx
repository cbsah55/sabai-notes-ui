import React, {useEffect, useState} from 'react';
import axiosInterceptor from "../../../api/axiosInterceptor";
import {Autocomplete} from "@material-ui/lab";
import {CircularProgress, TextField} from "@material-ui/core";

const AutoCompleteSearch = (props) => {
    const [open, setOpen] = React.useState(false);
    const [options, setOptions] = React.useState([]);
    const loading = open && options.length === 0;
    const [searchParam, setSearchParam] = useState();
    const id = 0;
    const {label, fieldId} = props;

    useEffect(() => {
        let active = true;
        if (!loading) {
            return undefined;
        }

        return () => {
            active = false;
        }

    }, [loading, searchParam]);

    useEffect(() => {
        if (!open) {
            setOptions([]);
        }
    }, [open]);

    const handleOnChange = (e) => {
        setSearchParam(e.target.value)
        setTimeout(() => {
            console.log("delayed");
        }, 2000);
        axiosInterceptor.get(`/public/${props.id}?filter=${searchParam}`)
            .then((res) => {
                const data = res.data.data;
                console.log(data)
                if (data !== undefined | null) {
                    setOptions(data);
                    // setOptions(Object.keys(data).map((key)=>data[key]));
                }
            })
            .catch(err => console.log(err));
    };

    const handleOptionChange = (value) => {
        value && props.callBackValue(value.id);
    };

    return (
        <Autocomplete
            id={props.id}
            open={open}
            onOpen={() => {
                setOpen(true);
            }}
            onClose={() => {
                setOpen(false);
            }}
            getOptionSelected={(option, value) => option.name === value.name}
            getOptionLabel={(option) => option.name}
            onChange={(event, value) => handleOptionChange(value)}
            options={options}
            loading={loading}
            renderInput={(params) => (
                <TextField
                    {...params}
                    label={props.label}
                    fullWidth
                    helperText="Start typing to see suggestions."
                    onChange={handleOnChange}
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <React.Fragment>
                                {loading ? <CircularProgress color="inherit" size={20}/> : null}
                                {params.InputProps.endAdornment}
                            </React.Fragment>
                        ),
                    }}
                />
            )}
        />
    );
};

export default AutoCompleteSearch;