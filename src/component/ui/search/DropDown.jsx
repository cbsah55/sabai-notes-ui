import React, {useEffect, useState} from 'react';
import {FormControl, FormHelperText, InputLabel, makeStyles, Select} from "@material-ui/core";
import axiosInterceptor from "../../../api/axiosInterceptor";

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
    },

}));
const DropDown = (props) => {
    const classes = useStyles();
    const [options, setOptions] = useState();
    const [error, setError] = useState();
    const [value, setValue] = useState(props.value);
    const {paramValue} = props;

    useEffect(() => {
        console.log(paramValue)
        axiosInterceptor.get(`/secure/${props.uri}/${props.id}`,
            {
                params: {
                    id: paramValue
                }
            })
            .then((response) => {
                const values = response.data.data;
                setOptions(values);
            })
            .catch(error => setError(error));
    }, [paramValue]);


    const handleChange = (event) => {
        const id = event.target.value;
        props.callBackValue(id);
        setValue(event.target.key);
    };
    return (
        <FormControl className={classes.formControl}>
            <InputLabel shrink htmlFor="age-native-simple">{props.label}</InputLabel>
            <Select
                native
                value={value}
                onChange={handleChange}
                inputProps={{
                    name: 'faculty',
                    id: `age-native-simple`,
                }}

            >
                <option aria-label="None" value=""/>
                {options && options.map((value) => {
                    return <option key={value.name} value={value.id}>
                        {value.name}
                    </option>
                })}
            </Select>
            <FormHelperText>Click to choose From Options</FormHelperText>
        </FormControl>
    );
};

export default DropDown;