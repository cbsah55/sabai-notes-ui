import React from 'react';
import {alpha, makeStyles} from "@material-ui/core";
import {blue, blueGrey} from "@material-ui/core/colors";

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
    sideBarRoutesWrapper: {
        background: "#efefef",
        display: "flex",
        flexFlow: "column",
        minHeight: "95vh",
        padding: theme.spacing(4, 2, 2, 32)
    },
    //sidebar styles
    sideNavRoot: {
        display: 'flex',
    },
    drawerPaper: {
        width: drawerWidth,
        marginTop: theme.spacing(7),

    },

    //AppBar styles
    appBar: {
        backgroundColor: '#fafafa',
        maxHeight: '48px',
        flexGrow: 1,
        elevation : 2,
        marginBottom: 10
    },
    appToolbar: {
        justifyContent: "space-between",
        maxHeight: "48px",
        minHeight: '46px'
    },
    appBarBox: {
        display: "flex",
        flexWrap: "inherit",
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    appBarLogo: {
        width: "160px",
        objectFit: "cover",
        maxWidth: "160px",
        marginTop: "5px",
    },
    navLinks: {
        color: blue["A800"],
        "& :hover,& :hover div": {
            color: blue["A200"],
        },
        "& div": {
            color: blueGrey["A800"],
        },
    },
    activeNavLinks: {
        backgroundColor: blueGrey[50],
        color: blue['A700'],
        "& div": {
            color: blue["A700"],
        },
    },
    navButton: {
        width: " 100%",
        textTransform: "capitalize",
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: alpha(theme.palette.common.black, 0.15),
        '&:hover': {
            backgroundColor: alpha(theme.palette.common.black, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: "50%",
        },
    },
    inputRoot: {
        color: 'inherit',
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputInput: {
        color: "black",
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('lg')]: {
            width: '20ch',
        },
    },
}));

export default useStyles;