import React from 'react';

function Constants(){
    const mySubjects =[
        {
            id: 1,
            name:"Applied Fuse Mechanics",
            short:"AFM"
        },
        {
            id: 2,
            name:"Information System",
            short:"IS"
        },
        {
            id: 3,
            name:"Artificial Intelligence",
            short:"AI"
        },
        {
            id: 4,
            name:"Software engineering",
            short:"SE"
        }
        ];

    const myRecentActivities =[
        {
            id: 1,
            name:"Notes for Software engineering",
            page:"5"
        },
        {
            id: 2,
            name:"Notes for Applied engineering",
            page:"67"
        },
        {
            id: 2,
            name:"Notes for Software Industry",
            page:"5"
        },
        {
            id: 26,
            name:"Notes for Soil Mechanics",
            page:"5"
        },
    ];
    const faq=[
    {
        title: "Q1. What can be uploaded?",
            description:"Any educational content, which you think can be useful to other students or teachers can be uploaded.\n" +
    "\n" +
    "\n" +
    "\n" +
    "You can upload Notes, Questions, Solutions, Case Studies, Lab Manuals, Project Report, Seminar Presentation, Research Papers and anything and everything related to education, Not necessarily limited to engineering."
    },
    {
        title: "Q2. What document formats are accepted?",
            description:"AWe accept pdf, word( doc, docx ), Powerpoint Presentation, ppt, pptx, Open office document( odt ), jpeg, " +
    "png and every other document format. If you want to upload some other format, please contact us."
    },

    {
        title: "Q3. How long it takes for the upload to finish?",
            description:"It depends on the size and speed of your Internet. It generally takes a maximum of 5 minutes for your document to get online and be available to the world."
    },
    {
        title: "Q4. what is the approval process and how does it work?",
            description:"To make sure only clean contents come online at LectureNotes, all uploads go through a small approval process. It usually takes 30 minutes but can take up to 24 hours for approval. if you want immediate approval please contact us."
    },
    {
        title: "Q5. I am not a teacher, can I upload?",
            description:"Our service ia an educational network,and in a network every member is important.You can upload all your notes and makes it available, for all your friends and other students worldwide."
    },

];

    return {mySubjects,myRecentActivities,faq}
};

export default Constants;


