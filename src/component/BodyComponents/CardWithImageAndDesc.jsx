import React from 'react';
import {Box, Card, CardActionArea, makeStyles, Typography} from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    cardRoot:{
        elevation : 4,
        borderRadius: '26px',
        position: "relative",
        width: '100%',
        overflow: "hidden"
    },
    cardMedia:{
        width : "100%",
        height : 150,
        position: 'center',
        objectFit: "fill"
    },
    cardContent:{
        display : "flex",
        padding : 4,
        maxHeight : "fit-content",
        alignItems : "center",
        flexDirection : "column",
    },
    cardTitle:{
        display : "block",
        textAlign: "inherit",
        color : '#102b45',
        fontSize : "medium",
        padding: 5
    },
    cardDescription: {
        fontSize: "small",
        textAlign: "center",
        color: 'rgba(0,0,0,0.54)',
        boxSizing: "inherit",
        display: "block",
        padding: 5

    },
}));
const CardWithImageAndDesc = (props) => {
    const classes = useStyles();
    return (
        <Card className={classes.cardRoot}>
            <CardActionArea component={Link} to={{
                pathname: "/materialList",
                search: `?type=${props.type}`,
                state:
                    {
                        id: props.id,
                        type: props.type,
                    }
            }}>
                <Typography>
                    <img src={props.image} className={classes.cardMedia}/>
                </Typography>
                <CardContent className={classes.cardContent}>
                    <Box className={classes.cardTitle}>{props.title}</Box>
                    <Box className={classes.cardDescription}>{props.description}</Box>
                </CardContent>
            </CardActionArea>
        </Card>
    );
};

export default CardWithImageAndDesc;