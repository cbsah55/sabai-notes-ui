import React, {useEffect, useState} from 'react';
import {Box, Card, CardActionArea, Grid, Typography} from "@material-ui/core";
import DropDown from "../../ui/search/DropDown";
import CardContent from "@material-ui/core/CardContent";
import axiosInterceptor from "../../../api/axiosInterceptor";
import {useHistory} from "react-router-dom";

const Courses = () => {
    const history = useHistory();
    const [fieldOfStudyId, setFieldOfStudyId] = useState('');
    const [courseId, setCourseId] = useState('');
    const [courseFacultyId, setCourseFacultyId] = useState('');
    const [dataList, setDataList] = useState();
    const [error, setError] = useState();
    const [isSubject, setIsSubject] = useState(false);

    useEffect(() => {
        axiosInterceptor.get(`/secure/user/field-of-studies/`)
            .then((response) => {
                const values = response.data.data;
                setDataList(values);
            })
            .catch(error => setError(error));
    }, [fieldOfStudyId]);

    useEffect(() => {
        axiosInterceptor.get(`/secure/user/courses/`, {params: {id: fieldOfStudyId}})
            .then((response) => {
                const values = response.data.data;
                setDataList([]);
                setDataList(values);
            })
            .catch(error => setError(error));
    }, [fieldOfStudyId]);

    useEffect(() => {
        axiosInterceptor.get(`/secure/user/course-faculty/`, {params: {id: courseId}})
            .then((response) => {
                const values = response.data.data;
                setDataList([]);
                setDataList(values);
            })
            .catch(error => setError(error));
    }, [courseId]);

    useEffect(() => {
        axiosInterceptor.get(`/secure/user/course-faculty-subjects/`, {params: {id: courseFacultyId}})
            .then((response) => {
                const values = response.data.data;
                setDataList([]);
                setDataList(values);
                setIsSubject(true);
            })
            .catch(error => setError(error));
    }, [courseFacultyId]);

    const handleCoursesCallBack = (value) => {
        setCourseId(value);
        setIsSubject(false);
    };

    const handleFieldOfStudiesCallback = (value) => {
        setFieldOfStudyId(value);
        setIsSubject(false);
    };

    const handleCourseFacultyCallBack = (value) => {
        setCourseFacultyId(value);
    };

    const handleCardClick = (id, name) => {
        console.log(id);
        isSubject && history.push({
            pathname: `/subjectDetails/${id}`,
            state: {
                id: id,
                name: name,
            }
        });
    };
    return (
        <>
            <Typography variant={"h5"}>Your Courses</Typography>
            <Typography variant={"body2"}>We have gathered all your courses here</Typography>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={3} lg={4}>
                    <DropDown id={"field-of-studies"} uri={"user"} label={" Field Of studies"}
                              callBackValue={handleFieldOfStudiesCallback}/>
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={4}>
                    <DropDown id={"courses"} uri={"user"} label={"Courses"} paramValue={fieldOfStudyId}
                              callBackValue={handleCoursesCallBack}/>
                </Grid>
                <Grid item xs={12} sm={6} md={3} lg={4}>
                    <DropDown id={"course-faculty"} uri={"user"} label={"Course Faculty"} paramValue={courseId}
                              callBackValue={handleCourseFacultyCallBack}/>
                </Grid>

                <Grid item xs={12} sm={12}>

                    <Box display={"flex"}>
                        {dataList && dataList.map((item, i) =>
                            <Box margin={2}>
                                <Card style={{
                                    padding: "1px",
                                    backgroundColor: "rgb(0,203,188)",
                                    width: "150px",
                                    height: "120px",
                                }}
                                >
                                    <CardActionArea onClick={() => handleCardClick(item?.id, item?.name)}>
                                        <CardContent>
                                            <Typography variant={"body1"}>{item?.name} </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>
                            </Box>
                        )}

                    </Box>
                </Grid>
            </Grid>


        </>
    );
};

export default Courses;