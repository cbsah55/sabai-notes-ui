import React, {useEffect, useState} from 'react';
import axiosInstance from "../../../api/axiosInterceptor";
import {Skeleton} from "@material-ui/lab";
import ErrorSnackBar from "../../header/ErrorSnackBar";
import {Grid, Typography} from "@material-ui/core";
import MyRecentActivitiesCard from "../../recentActivities/MyRecentActivitiesCard";
import DescriptionIcon from "@material-ui/icons/Description";

const MyFavourite = () => {
    const [response, setResponse] = useState();
    const [error, setError] = useState();
    const [loading, setLoading] = useState();


    useEffect(() => {
        setLoading(true);
        axiosInstance.get("/secure/user/favourites")
            .then((res) => {
                console.log(res.data.data);
                setResponse(res.data.data);
            })
            .catch(error => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();
                setError(resMessage);
            });

    }, []);

    return (
        response == null | undefined ? <>
                <Skeleton animation={"wave"} style={{paddingTop: "30px", marginTop: "30px"}}/>
                <Skeleton animation={"wave"} style={{paddingTop: "30px", marginTop: "30px"}}/>
                <Skeleton animation={"wave"} style={{paddingTop: "30px", marginTop: "30px"}}/>
                <Skeleton animation={"wave"} style={{paddingTop: "30px", marginTop: "30px"}}/>
            </> :
            <>
                {error.length > 0 && <ErrorSnackBar error={error}/>}
                <Typography variant={"h2"} gutterBottom>My Favourites</Typography>
                <Grid container spacing={2}>
                    {
                        response && response.length > 0 ? response.map(material =>
                                <Grid item xs={12} md={6} sm={12}>
                                    <MyRecentActivitiesCard id={material.id} topic={material.name}
                                                            imageIcon={DescriptionIcon}/>
                                </Grid>
                            )
                            :
                            <Grid item>
                                <p>No Results Found</p>
                            </Grid>
                    }
                </Grid>

            </>
    );
};

export default MyFavourite;