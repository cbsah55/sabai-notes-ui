import React from 'react';
import {Box, Grid, Link, makeStyles, Typography} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    gridItem: {
        width: "100%",
        margin: theme.spacing(0,0,2,0)
    },
    mySubjectTypography: {
        boxSizing: "inherit"
    },
    subjectBox: {
        padding: "8px",
        boxShadow:
            "rgb(0 0 0 / 20%) 0px 3px 1px -2px, rgb(0 0 0 / 14%) 0px 2px 2px 0px, rgb(0 0 0 / 12%) 0px 1px 5px 0px",
        borderRadius: "10px",
        backgroundColor: "rgb(0, 203, 188)",
        boxSizing: "inherit",
        display: "block"
    },
    link:{
        color: "white"
    },
}));
const MySubjects = ({shortName,fullName,id}) => {
    const classes = useStyles();

    const preventDefault=(e)=> {
            e.preventDefault();
    };

    return (
        <div>
                <Grid item className={classes.gridItem}>
                    <Typography className={classes.mySubjectTypography}>
                        <Link href={"https://googele.com"} onClick={preventDefault} className={classes.link} underline={"none"}>
                    <Box className={classes.subjectBox}>
                        <Box display={"block"} fontSize={'18px'} fontWeight={700}>
                            {shortName}
                        </Box>
                        <Box display={"block"} fontSize={'14px'} fontWeight={400}>
                            {fullName}
                        </Box>
                    </Box>
                        </Link>
                    </Typography>
                </Grid>
        </div>
    );
};

export default MySubjects;