import React from 'react';
import {Box, Grid, makeStyles} from "@material-ui/core";
import prime from '../../../static/prime_notes.svg';
import PreviousQuestion from '../../../static/Question.svg';
import MCQ from '../../../static/mcq.svg';
import QB from '../../../static/Question_Bank.svg';
import MySubjects from "./MySubjects";
import CardWithImageAndDesc from "../CardWithImageAndDesc";
import TitleSm from "../../ui/TitleSm";
import MyRecentActivitiesCard from "../../recentActivities/MyRecentActivitiesCard";
import DescriptionIcon from '@material-ui/icons/Description';
import Constants from "../../Constants";


const useStyles = makeStyles((theme) => ({}));
const Home = () => {
    const {mySubjects, myRecentActivities} = Constants();
    return (
        <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={8} lg={8}>
                <Box>
                    <Grid container spacing={2}>
                        <Grid item xs={12} md={12} sm={6} lg={6}>
                            <CardWithImageAndDesc
                                title={"Prime Notes"}
                                image={prime}
                                description={"Curated notes to provide a better understanding & knowledge on any topic"}
                                link={"https://google.com"}/>
                        </Grid>
                        <Grid item xs={12} md={12} sm={6} lg={6}>
                            <CardWithImageAndDesc
                                title={"Previous Year Questions"}
                                image={PreviousQuestion}
                                description={"Practice previous year questions & get more significant content for any subject."}/>
                        </Grid>
                        <Grid item xs={12} md={12} sm={6} lg={6}>
                            <CardWithImageAndDesc
                                title={"MCQ"}
                                image={MCQ}
                                description={"Evaluate Yourself based on your performance and enhance your learning."}/>
                        </Grid>
                        <Grid item xs={12} md={12} sm={6} lg={6}>
                            <CardWithImageAndDesc
                                title={"Question Bank"}
                                image={QB}
                                description={"Get Hand-picked questions with answers for all topics as per your syllabus."}/>
                        </Grid>
                    </Grid>
                </Box>
            </Grid>
            <Grid item xs={4} sm={12} md={4} lg={4}>
                <Box>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <TitleSm title={"My Subjects"}/>
                            {mySubjects && mySubjects.map(item =>
                                <MySubjects shortName={item.short} fullName={item.name} id={item.id}/>
                            )}
                        </Grid>
                        <Grid item xs={12} spacing={2}>
                            <TitleSm title={"My Recent Activities"}/>
                            {myRecentActivities && myRecentActivities.map(activities =>
                                <MyRecentActivitiesCard id={activities.id} topic={activities.name}
                                                        imageIcon={DescriptionIcon}/>
                            )}
                        </Grid>
                    </Grid>
                </Box>
            </Grid>
        </Grid>
    );
};

export default Home;