import React, {useEffect, useState} from 'react';
import axiosInterceptor from "../../../../api/axiosInterceptor";
import {Box, Button, Card, Grid, TextField, Typography} from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";

const AddInstituteForm = () => {
    const [name, setName] = useState("");
    const [shortName, setShortName] = useState("");
    const [institutes, setInstitutes] = useState([]);
    const [error, setError] = useState();
    const [reload, setReload] = useState(false);

    useEffect(() => {
        axiosInterceptor.get("/secure/mod/institutes")
            .then((res) => {
                setInstitutes(res.data.data);
                console.log(res)
            })
            .catch((err) => {
                setError(err);
            }).finally(setReload(false))
    }, [reload])


    const handleSubmit = () => {
        const formData = new FormData();
        formData.append("name", name);
        axiosInterceptor.post("/secure/mod/institute", {
            name,
            shortName,

        })
            .catch(err => setError(err))
            .finally(setReload(true));
        handleCancel();

    };

    const handleNameChange = (e) => {
        setName(e.target.value);
    };

    const handleShortNameChange = (e) => {
        setShortName(e.target.value)
    };

    const handleCancel = () => {
        setName("");
        setShortName("");
    };

    return (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="name"
                        name="name"
                        label="Name"
                        value={name}
                        onChange={handleNameChange}
                        fullWidth
                        autoComplete="Subject name..."
                        helperText="eg. TU, PU with full name"
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="shortName"
                        name="shortName"
                        label="Short Name"
                        value={shortName}
                        onChange={handleShortNameChange}
                        fullWidth
                        autoComplete="Subject name..."
                        helperText="eg. PU for Purwanchal University"
                    />
                </Grid>


                <Grid item xs={12} sm={12}>
                    <Box display="flex" justifyContent={"space-between"}>
                        <Box>
                            <Button variant={"contained"} color={"secondary"} size={"small"}
                                    onClick={handleCancel}>Cancel</Button>
                        </Box>
                        <Box>
                            <Button variant={"contained"} size={"small"} color={"primary"}
                                    onClick={handleSubmit}>Submit</Button>
                        </Box>
                    </Box>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Typography>Available Institutes</Typography>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Box display={"flex"}>
                        {institutes && institutes.map((item, i) =>
                            <Box margin={2}>
                                <Card style={{
                                    padding: "5px",
                                    backgroundColor: "rgb(0,203,188)",
                                    maxWidth: "fit-content"
                                }}>
                                    <CardContent>
                                        <Typography variant={"body1"}>{item?.name}</Typography>
                                    </CardContent>
                                </Card>
                            </Box>
                        )}

                    </Box>
                </Grid>
            </Grid>
        </React.Fragment>
    );
};

export default AddInstituteForm;