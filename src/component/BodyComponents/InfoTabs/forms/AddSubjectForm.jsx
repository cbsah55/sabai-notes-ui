import React, {useEffect, useState} from 'react';
import {Box, Button, Card, Grid, TextField, Typography} from "@material-ui/core";
import AutoCompleteSearch from "../../../ui/search/AutoCompleteSearch";
import axiosInterceptor from "../../../../api/axiosInterceptor";
import SnackBar from "../../../header/ErrorSnackBar";
import CardContent from "@material-ui/core/CardContent";

const AddSubjectForm = () => {
    const [name, setName] = useState("");
    const [facultyId, setFacultyId] = useState("");
    const [shortName, setShortName] = useState("");
    const [subjects, setSubjects] = useState([]);
    const [successMessage, setSuccessMessage] = useState();
    const [error, setError] = useState();
    const [reload, setReload] = useState(false);


    useEffect(() => {
        axiosInterceptor.get("/secure/mod/subjects")
            .then((res) => {
                setSubjects(res.data.data);
            })
            .catch(err => setError(err))
            .finally(setReload(false))
    }, [reload])


    const handleSubmit = async () => {
        await axiosInterceptor.post("/secure/mod/subject", {
            name,
            facultyId,
            shortName

        }).then(response =>
            <SnackBar error={response.data.message}/>
        );
        setReload(true);
        handleCancel();

    };
    const callBackValue = (id) => {
        setFacultyId(id);
    };

    const handleNameChange = (e) => {
        setName(e.target.value);
    };

    const handleShortNameChange = (e) => {
        setShortName(e.target.value);
    };

    const handleCancel = () => {
        setName("");
        setShortName("");
    };
    return (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="name"
                        name="name"
                        label="Name"
                        value={name}
                        onChange={handleNameChange}
                        fullWidth
                        autoComplete="Subject name..."
                        helperText="eg. Artificial Intelligence, Data Mining & Data Warehouse"

                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="shortname"
                        name="shortName"
                        label="Short Name"
                        value={shortName}
                        onChange={handleShortNameChange}
                        fullWidth
                        autoComplete="Subject name..."
                        helperText="eg. AI, DMDW, SE"
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <AutoCompleteSearch label={"Faculty"} id={"faculties"} callBackValue={callBackValue}/>
                </Grid>

                <Grid item xs={12} sm={12}>
                    <Box display="flex" justifyContent={"space-between"}>
                        <Box>
                            <Button variant={"contained"} color={"secondary"} size={"small"}
                                    onClick={handleCancel}>Cancel</Button>
                        </Box>
                        <Box>
                            <Button variant={"contained"} size={"small"} color={"primary"}
                                    onClick={handleSubmit}>Submit</Button>
                        </Box>
                    </Box>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Typography>Available Subjects</Typography>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Box display={"flex"}>
                        {subjects && subjects.map((item, i) =>
                            <Box margin={2}>
                                <Card style={{
                                    padding: "5px",
                                    backgroundColor: "rgb(0,203,188)",
                                    maxWidth: "fit-content"
                                }}>
                                    <CardContent>
                                        <Typography variant={"body1"}>{item?.name}</Typography>
                                    </CardContent>
                                </Card>
                            </Box>
                        )}

                    </Box>
                </Grid>
            </Grid>
        </React.Fragment>
    );
};

export default AddSubjectForm;