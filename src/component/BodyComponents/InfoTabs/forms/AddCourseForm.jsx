import React, {useEffect, useState} from 'react';
import {Box, Button, Card, Grid, TextField, Typography} from "@material-ui/core";
import axiosInterceptor from "../../../../api/axiosInterceptor";
import CardContent from "@material-ui/core/CardContent";
import DropDown from "../../../ui/search/DropDown";


const AddCourseForm = () => {
    const [name, setName] = useState("");
    const [fieldOfStudyId, setFieldOfStudyId] = useState("");
    const [courses, setCourses] = useState([]);
    const [error, setError] = useState();
    const [reload, setReload] = useState(false);

    useEffect(() => {
        axiosInterceptor.get("/secure/mod/courses")
            .then((res) => {
                setCourses(res.data.data);
                console.log(res)
            })
            .catch(err => setError(err))
            .finally(setReload(false));
    }, [reload])


    const handleSubmit = async () => {
        const {response} = await axiosInterceptor.post("/secure/mod/course", {
            name,
            fieldOfStudyId,

        });
        console.log(response);
        setReload(true)

    };
    const onSelect = (id) => {
        setFieldOfStudyId(id);
    };

    const handleChange = (e) => {
        setName(e.target.value);
    };

    const handleCancel = () => {
        setName("");
    };

    return (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="name"
                        name="name"
                        label="Name"
                        value={name}
                        onChange={handleChange}
                        fullWidth
                        autoComplete="Subject name..."
                        helperText="eg. BE , ME, MCA for engineering "
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <DropDown id={'field-of-studies'} uri={"mod"} label={"Field Of Studies"} callBackValue={onSelect}/>
                </Grid>

                <Grid item xs={12} sm={12}>
                    <Box display="flex" justifyContent={"space-between"}>
                        <Box>
                            <Button variant={"contained"} color={"secondary"} size={"small"}
                                    onClick={handleCancel}>Cancel</Button>
                        </Box>
                        <Box>
                            <Button variant={"contained"} size={"small"} color={"primary"}
                                    onClick={handleSubmit}>Submit</Button>
                        </Box>
                    </Box>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Typography>Available courses</Typography>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Box display={"flex"}>
                        {courses && courses.map((item, i) =>
                            <Box margin={2}>
                                <Card style={{
                                    padding: "5px",
                                    backgroundColor: "rgb(0,203,188)",
                                    maxWidth: "fit-content"
                                }}>
                                    <CardContent>
                                        <Typography variant={"body1"}>{item?.name}</Typography>
                                    </CardContent>
                                </Card>
                            </Box>
                        )}

                    </Box>
                </Grid>
            </Grid>
        </React.Fragment>
    );
};

export default AddCourseForm;