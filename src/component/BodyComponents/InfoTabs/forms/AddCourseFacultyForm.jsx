import React, {useEffect, useState} from 'react';
import {Box, Button, Card, Grid, TextField, Typography} from "@material-ui/core";
import axiosInterceptor from "../../../../api/axiosInterceptor";
import CardContent from "@material-ui/core/CardContent";
import DropDown from "../../../ui/search/DropDown";


const AddCourseForm = () => {
    const [name, setName] = useState("");
    const [courseId, setCourseId] = useState("");
    const [shortName, setShortName] = useState("");
    const [faculties, setFaculties] = useState([]);
    const [successMessage, setSuccessMessage] = useState();
    const [error, setError] = useState();
    const [reload, setReload] = useState(false);

    useEffect(() => {
        axiosInterceptor.get("/secure/mod/course-faculties")
            .then((res) => {
                setFaculties(res.data.data);
            })
            .catch(err => setError(err))
            .finally(setReload(false));
    }, [reload]);


    const handleSubmit = async () => {
        await axiosInterceptor.post("/secure/mod/course-faculty", {
            name,
            courseId,
            shortName

        }).catch(error => setError(error));
        setReload(true);

    };
    const onSelect = (id) => {
        setCourseId(id);
    };

    const handleNameChange = (e) => {
        setName(e.target.value);
    };

    const handleShortNameChange = (e) => {
        setShortName(e.target.value);
    };

    const handleCancel = () => {
        setName("");
    };

    return (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="name"
                        name="name"
                        label="Name"
                        value={name}
                        onChange={handleNameChange}
                        fullWidth
                        autoComplete="Subject name..."
                        helperText="eg, Computer Engineering,Civil Engineering"
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="shortname"
                        name="shortName"
                        label="Short Name"
                        value={shortName}
                        onChange={handleShortNameChange}
                        fullWidth
                        autoComplete="Subject name..."
                        helperText="eg. BCE for Bachelor of computer engineering"
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <DropDown id={'courses'} uri={"mod"} label={"Course"} callBackValue={onSelect}/>
                </Grid>

                <Grid item xs={12} sm={12}>
                    <Box display="flex" justifyContent={"space-between"}>
                        <Box>
                            <Button variant={"contained"} color={"secondary"} size={"small"}
                                    onClick={handleCancel}>Cancel</Button>
                        </Box>
                        <Box>
                            <Button variant={"contained"} size={"small"} color={"primary"}
                                    onClick={handleSubmit}>Submit</Button>
                        </Box>
                    </Box>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Typography>Available Faculties</Typography>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Box display={"flex"}>
                        {faculties && faculties.map((item, i) =>
                            <Box margin={2}>
                                <Card style={{
                                    padding: "5px",
                                    backgroundColor: "rgb(0,203,188)",
                                    maxWidth: "fit-content"
                                }}>
                                    <CardContent>
                                        <Typography variant={"body1"}>{item?.name}</Typography>
                                    </CardContent>
                                </Card>
                            </Box>
                        )}

                    </Box>
                </Grid>
            </Grid>
        </React.Fragment>
    );
};

export default AddCourseForm;