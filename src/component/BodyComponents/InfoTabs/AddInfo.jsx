import React from 'react';
import {AppBar, Box, makeStyles, Tab, Tabs, Typography, useTheme} from "@material-ui/core";
import PropTypes from "prop-types";
import SwipeableViews from 'react-swipeable-views';
import AddSubjectForm from "./forms/AddSubjectForm";
import AddCourseForm from "./forms/AddCourseForm";
import AddInstituteForm from "./forms/AddInstituteForm";
import AddFieldOfStudyForm from "./forms/AddFieldOfStudyForm";
import AddCourseFacultyForm from "./forms/AddCourseFacultyForm";


function TabPanel(props) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,

    },

}));
const AddInfo = () => {
    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    return (
        <div className={classes.root}>
            <AppBar position="static" color="default" style={{justifyContent: "space-between"}}>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"

                >
                    <Tab label="Add Institutes" {...a11yProps(0)} />
                    <Tab label="Field Of Studies" {...a11yProps(1)} />
                    <Tab label="Add courses" {...a11yProps(2)} />
                    <Tab label="Add faculty" {...a11yProps(3)} />
                    <Tab label="Add subjects" {...a11yProps(4)} />
                </Tabs>
            </AppBar>
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                <TabPanel value={value} index={0} dir={theme.direction}>
                    <AddInstituteForm/>
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                    <AddFieldOfStudyForm/>
                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                    <AddCourseForm/>
                </TabPanel>
                <TabPanel value={value} index={3} dir={theme.direction}>
                    <AddCourseFacultyForm/>
                </TabPanel>
                <TabPanel value={value} index={4} dir={theme.direction}>
                    <AddSubjectForm/>
                </TabPanel>
            </SwipeableViews>
        </div>
    );
};

export default AddInfo;