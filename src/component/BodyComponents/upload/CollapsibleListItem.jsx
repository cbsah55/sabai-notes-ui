import React, {useState} from 'react';
import List from "@material-ui/core/List";
import {Collapse, ListItem, ListItemText, Typography} from "@material-ui/core";
import {ExpandLess, ExpandMore} from "@material-ui/icons";

const CollapsibleListItem = ({title,desc}) => {

    const [ open,setOpen] = useState(false);
    function handleClick() {
        setOpen(!open);
    }

    return (
        <div>
        
            <ListItem button onClick={handleClick}>
                <ListItemText primary={title} />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <ListItem button>
                        <Typography variant={"body1"}>{desc}</Typography>
                    </ListItem>
                </List>
            </Collapse>
        </div>
);
};

export default CollapsibleListItem;