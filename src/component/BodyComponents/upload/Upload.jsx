import {Box, Button, Card, CardHeader, Grid, makeStyles} from "@material-ui/core";
import {DropzoneArea} from 'material-ui-dropzone';
import CardContent from "@material-ui/core/CardContent";
import List from "@material-ui/core/List";
import React, {createRef, useEffect, useState} from "react";
import CollapsibleListItem from "./CollapsibleListItem";
import useFileUpload from '../../../hooks/fileUpload/useFileUpload';
import TitleSm from "../../ui/TitleSm";
import Constants from "../../Constants";
import MyRecentActivitiesCard from "../../recentActivities/MyRecentActivitiesCard";
import DescriptionIcon from "@material-ui/icons/Description";
import useMaterialFetch from "../../../hooks/material/useMaterialFetch";
import CircularProgress from "@material-ui/core/CircularProgress";
import ErrorSnackBar from "../../header/ErrorSnackBar";
import SuccessSnackBar from "../../header/SuccessSnackBar";

const useStyles = makeStyles((theme) => ({
    card: {
        elevation: 0,
        borderRadius: '10px',
        padding: 10,
    },
    cardHeader: {
        textAlign: "center",
        fontSize: 20,
        fontWeight: 600,
        height: 30
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
}));
const Upload = () => {
    const classes = useStyles();
    const dropzoneRef = createRef();
    const {faq} = Constants();
    const [tempFile, setTempFile] = useState();
    const [file, setFile] = useState();
    const {response: uploadResponse, loading, error, successMessage} = useFileUpload(file);
    const {response: materials} = useMaterialFetch();
    const [data, setData] = useState('');



    useEffect(() => {
        if (uploadResponse !== null) {
            setData(uploadResponse);
        }
    }, [uploadResponse]);

    const handleChange = (files) => {
        console.log('Files', files)
        setTempFile(files[0]);
    };

    function handleCancel() {
        setTempFile(undefined);
    }

    function handleUpload() {
        setFile(tempFile);
    }
    return (
        <>
            <Grid container>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={12} md={8} lg={8}>
                        <Box>
                            <Grid container>
                                <Grid item xs={12} sm={12} md={12} lg={12}>
                                    <Card style={{
                                        padding: 10,
                                        borderRadius: '10px',
                                    }}>
                                        <Card className={classes.card}>
                                            <DropzoneArea
                                                ref={dropzoneRef}
                                                acceptedFiles={['image/*', 'video/*', 'application/*']}
                                                onChange={(files) => handleChange(files)}
                                                onDelete={handleCancel}
                                                showFileNames
                                                dropzoneText="Drag and drop an image or click to select files. "
                                                showAlerts={true}
                                                filesLimit={1}
                                            />
                                            <Box display="flex" justifyContent={"space-between"} paddingTop="5px">
                                                <Box>
                                                    <Button variant="contained" color="secondary"
                                                            onClick={handleCancel}>
                                                        Cancel
                                                    </Button>
                                                </Box>
                                                <Box>
                                                    {loading && <CircularProgress size={24}/>}
                                                    {error.length > 0 && <ErrorSnackBar error={error}/>}
                                                    {successMessage.length > 0 &&
                                                    <SuccessSnackBar successMessage={successMessage}/>}
                                                </Box>
                                                <Box>
                                                    <Button variant="contained" color="primary"
                                                            onClick={() => handleUpload()}>
                                                        Upload
                                                    </Button>
                                                </Box>
                                            </Box>
                                        </Card>
                                    </Card>
                                </Grid>
                            </Grid>
                            {/*//upload section finish*/}
                        </Box>
                    </Grid>

                    {/*faq section grid*/}
                    <Grid item xs={4} sm={12} md={4} lg={4}>
                        <Card>
                            <CardHeader className={classes.cardHeader}
                                        title={"FAQ"}/>
                            <CardContent>
                                <List>
                                    {faq && faq.map((item, i) =>
                                        <CollapsibleListItem title={item.title} desc={item.description}/>
                                    )}
                                </List>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
                {/*bottom grid*/}
                <Grid item md={8}>
                    <Grid container spacing={3}>
                        <Grid item md={12} sm={12} xs={12}>
                            <TitleSm title={"My Materials"}/>
                        </Grid>

                        {
                            materials && materials.slice(0, 15).map(material =>
                                <Grid item xs={12} md={6} sm={12}>
                                    <MyRecentActivitiesCard id={material.id} topic={material.name}
                                                            imageIcon={DescriptionIcon}/>
                                </Grid>
                            )}

                    </Grid>
                </Grid>
            </Grid>

        </>
    )
};


export default Upload;