import React, {useState} from 'react';
import {useLocation} from "react-router-dom";
import {Grid, Typography} from "@material-ui/core";
import axiosInstance from "../../api/axiosInterceptor";
import ErrorSnackBar from "../../component/header/ErrorSnackBar";
import MyRecentActivitiesCard from "../../component/recentActivities/MyRecentActivitiesCard";
import DescriptionIcon from "@material-ui/icons/Description";
import {Skeleton} from "@material-ui/lab";

const MaterialList = () => {
    const location = useLocation();
    const {id: subjectId, type} = location.state;
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
    const [response, setResponse] = useState();

    useState(() => {
        setLoading(true);
        axiosInstance.get("/secure/user/materials", {
            params: {
                subjectId: subjectId,
                type: type,
            }

        })
            .then((response) => {
                setResponse(response.data.data);
            })
            .catch(error => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();
                setLoading(false);
                setError(resMessage);
            });
        setLoading(false);
    });

    return (
        response == null | undefined ? <>
                <Skeleton animation={"wave"} style={{paddingTop: "30px", marginTop: "30px"}}/>
                <Skeleton animation={"wave"} style={{paddingTop: "30px", marginTop: "30px"}}/>
                <Skeleton animation={"wave"} style={{paddingTop: "30px", marginTop: "30px"}}/>
                <Skeleton animation={"wave"} style={{paddingTop: "30px", marginTop: "30px"}}/>
            </> :
            <>
                {error.length > 0 && <ErrorSnackBar error={error}/>}
                <Typography variant={"h2"} gutterBottom>Notes On</Typography>
                <Grid container spacing={2}>
                    {
                        response && response.length > 0 ? response.map(material =>
                                <Grid item xs={12} md={6} sm={12}>
                                    <MyRecentActivitiesCard id={material.id} topic={material.name}
                                                            imageIcon={DescriptionIcon}/>
                                </Grid>
                            )
                            :
                            <Grid item>
                                <p>No Results Found</p>
                            </Grid>
                    }
                </Grid>

            </>
    );
};

export default MaterialList;