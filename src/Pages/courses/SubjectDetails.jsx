import React from 'react';
import {useLocation} from "react-router-dom";
import {Box, Grid, Typography} from "@material-ui/core";
import CardWithImageAndDesc from "../../component/BodyComponents/CardWithImageAndDesc";
import prime from "../../static/prime_notes.svg";
import PreviousQuestion from "../../static/Question.svg";
import MCQ from "../../static/mcq.svg";
import QB from "../../static/Question_Bank.svg";

const SubjectDetails = () => {
    const location = useLocation();
    return (
        <>
            <Typography variant={"h2"} gutterBottom>{location?.state.name}</Typography>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={8} lg={8}>
                    <Box>
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={12} sm={6} lg={6}>
                                <CardWithImageAndDesc
                                    title={"Notes"}
                                    image={prime}
                                    description={"Curated notes to provide a better understanding & knowledge on any topic"}
                                    id={location.state.id}
                                    type={"Note"}
                                />
                            </Grid>
                            <Grid item xs={12} md={12} sm={6} lg={6}>
                                <CardWithImageAndDesc
                                    title={"Previous Year Questions"}
                                    image={PreviousQuestion}
                                    description={"Practice previous year questions & get more significant content for any subject."}
                                    id={location.state.id}
                                    type={"Prev_Year_Question"}
                                />
                            </Grid>
                            <Grid item xs={12} md={12} sm={6} lg={6}>
                                <CardWithImageAndDesc
                                    title={"Syllabus"}
                                    image={MCQ}
                                    description={"Evaluate Yourself based on your performance and enhance your learning."}
                                    id={location.state.id}
                                    type={"Syllabus"}/>
                            </Grid>
                            <Grid item xs={12} md={12} sm={6} lg={6}>
                                <CardWithImageAndDesc
                                    title={"Question Bank"}
                                    image={QB}
                                    description={"Get Hand-picked questions with answers for all topics as per your syllabus."}
                                    id={location.state.id}
                                    type={"Question_Bank"}/>
                            </Grid>
                        </Grid>
                    </Box>
                </Grid>
            </Grid>
        </>
    );
};

export default SubjectDetails;