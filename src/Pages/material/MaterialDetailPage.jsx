import React, {useEffect, useState} from 'react';
import {Link, useLocation} from "react-router-dom";
import {
    Box,
    Button,
    Card,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    ListItem,
    makeStyles,
    Paper,
    Typography
} from "@material-ui/core";
import List from "@material-ui/core/List";
import ShowBackDrop from "../../component/header/ShowBackDrop";
import ErrorSnackBar from "../../component/header/ErrorSnackBar";
import {pdfjs} from "react-pdf";
import PdfRender from "../../component/pdf/PdfRender";
import MyRating from "../../component/ui/material/Rating";
import axiosInterceptor from "../../api/axiosInterceptor";
import axiosInstance from "../../api/axiosInterceptor";
import MaterialDetailCard from "../../component/ui/material/MaterialDetailCard";
import {Skeleton} from "@material-ui/lab";
import useAuthService from "../../service/auth/useAuthService";

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;


const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(4),
        width: "100%",
        backgroundColor: "#e5e5e5"
    },
    title: {
        elevation: 0,
        height: "100%",
        display: "flex",
        padding: theme.spacing(2),
        marginBottom: 20,
        wordWrap: "initial"
    },
    cardSide: {
        padding: "5px",
        maxWidth: 400,
        height: "100%",
        width: "100%",
        margin: theme.spacing(0, 0, 3, 0)
    },
    cardHeader: {
        background: "#efefef",
        height: "30px",
        padding: "3px",
        fontSize: "medium",
    },
    cardImage: {
        elevation: 3,
        height: "400px",
        maxHeight: "400px",
        maxWidth: "100%",
        marginBottom: 20,
    }
}));

const MaterialDetailPage = () => {
    const classes = useStyles()
    const location = useLocation();
    const [response, setResponse] = useState();
    const [error, setError] = useState();
    const [loading, setLoading] = useState();
    const [id, setId] = useState();
    const {isAuthenticated} = useAuthService();

    useEffect(() => {
        setLoading(true);
        setId(location.state.id);
        axiosInstance.get("/public/materials", {params: {id: location.state.id}})
            .then((res) => {
                console.log(res.data.data);
                setResponse(res.data.data);
            })
            .catch((error) => {
                setError(error);
            })
            .finally(() => {
                setLoading(false);
            });

    }, []);


    const handleFavourite = () => {
        response?.favourite ?
            axiosInterceptor.delete("/secure/user/favourite", null, {params: {materialId: location.state.id}})
            :
            axiosInterceptor.post("/secure/user/favourite", null, {params: {materialId: location.state.id}})
                .then((response) => {
                    console.log(response.data.message);
                })
                .catch(error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();
                    setError(resMessage);
                });
    }

    const handleManage = () => {

    }

    return (
        response == null | undefined ? <>
                <Skeleton animation={"wave"} style={{paddingTop: "30px", marginTop: "30px"}}/>
                <Skeleton animation={"wave"} style={{paddingTop: "30px", marginTop: "30px"}}/>
                <Skeleton animation={"wave"} style={{paddingTop: "30px", marginTop: "30px"}}/>
                <Skeleton animation={"wave"} style={{paddingTop: "30px", marginTop: "30px"}}/>
            </> :
            <>
                <Grid container spacing={2} className={classes.root}>
                    <ShowBackDrop loading={loading}/>
                    {error && <ErrorSnackBar error={error}/>}
                    <Grid item xs={12}>
                        <Paper className={classes.title}>
                            <Grid container spacing={3} justifyContent={"space-between"}>
                                <Grid item md={8}>
                                    <Typography variant={"h6"} color={"inherit"}>{response.name}</Typography>
                                    <MyRating currValue={response.rating} id={response.id}/>
                                </Grid>
                                <Grid item md={4}>
                                    <Grid container spacing={1} justifyContent={"flex-end"}>
                                        <Grid item>
                                            {isAuthenticated &&
                                            <Button color={response?.favourite ? "secondary" : "primary"}
                                                    variant={"outlined"}
                                                    marginLeft={"5px"} onClick={handleFavourite}>Favourite</Button>}
                                        </Grid>
                                        {response?.owner ?
                                            <Grid item>
                                                <Link to={`/manage/${id}`} params={{id: id}}>
                                                    <Button color={"primary"} variant={"outlined"}
                                                            marginLeft={4}>Manage</Button>
                                                </Link>
                                            </Grid>
                                            :
                                            <Grid item>
                                            </Grid>
                                        }

                                    </Grid>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                    {/* //Title grid end*/}

                    {/*topic grid start*/}
                    <Grid item md={3} xs={12}>
                        <Card className={classes.cardSide}>
                            <CardHeader
                                title={"TOPICS"}
                                className={classes.cardHeader}
                            />
                            <CardContent style={{maxHeight: 400, overflow: 'auto'}}>
                                <List>
                                    {
                                        response?.subject?.subjectTopics?.length <= 0 ?
                                            <ListItem divider={true}>No any topics found</ListItem>
                                            :
                                            response?.subject?.subjectTopics?.map((topic =>
                                                    <ListItem divider={true}>{topic.name}</ListItem>
                                            ))}
                                </List>
                            </CardContent>
                        </Card>

                        <Card className={classes.cardSide}>
                            <CardHeader
                                title={"Suggested Materials"}
                                style={{fontSize: '10px', background: "#efefef"}}
                            />
                            <CardContent style={{maxHeight: 400, overflow: 'auto'}}>
                                // to add suggestions card
                            </CardContent>
                        </Card>
                    </Grid>

                    <Grid item xs={12} md={5}>
                        <Box position="fixed" textAlign={"center"}>
                            <Box>
                                <Card className={classes.cardImage}>
                                    <CardMedia>
                                        {response && <img src={response?.coverUrl} style={{
                                            objectFit: "contain"
                                        }}/>}
                                    </CardMedia>
                                </Card>
                            </Box>
                            <Box>
                                <PdfRender pdfUrl={response.url}/>
                            </Box>
                        </Box>
                    </Grid>
                    <Grid item md={4}>
                        <MaterialDetailCard response={response}/>
                    </Grid>

                </Grid>

            </>
    );
};

export default MaterialDetailPage;