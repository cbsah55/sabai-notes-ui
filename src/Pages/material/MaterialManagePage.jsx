import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import {Box, Button, Card, Grid, makeStyles, Typography} from "@material-ui/core";
import MaterialEditForm from "../../component/ui/material/MaterialEditForm";
import MaterialSwitches from "../../component/ui/material/MaterialSwitches";
import axiosInstance from "../../api/axiosInterceptor";
import ShowBackDrop from "../../component/header/ShowBackDrop";

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: "white"
    },
    title: {
        backgroundColor: "white",
        display: "flex",
        justifyContent: "center"
    },
    basicDetails: {
        height: "100%",
        width: 400,
        padding: 6
    }

}));
const MaterialManagePage = () => {
    const classes = useStyles();
    const [openEdit, setOpenEdit] = useState(false);
    const {id} = useParams();
    const [response, setResponse] = useState();
    const [error, setError] = useState();
    const [loading, setLoading] = useState();


    useEffect(() => {
        setLoading(true);
        axiosInstance.get("/secure/user/myMaterials", {params: {id}})
            .then((res) => {
                console.log(res.data.data);
                setResponse(res.data.data);
            })
            .catch((error) => {
                setError(error);
            })
            .finally(() => {
                setLoading(false);
            });
    }, []);


    const openEditForm = (value) => {
        setOpenEdit(value);
    };
    const handleSuccess = (value) => {
        setOpenEdit(value);
    };
    return (
        <>
            <ShowBackDrop loading={loading}/>
            <Typography variant={"h4"} display={"inline"} gutterBottom className={classes.title}>Manage
                material</Typography>
            <div>
                <Grid container spacing={4}>
                    <Grid item sm={12} md={12}>
                        <Typography variant={"h6"} display={"inline"}
                                    className={classes.title}>{response?.name}</Typography>
                    </Grid>
                    {!openEdit ?
                        <Grid item>
                            <Card className={classes.basicDetails}>
                                <Box display={"flex"} justifyContent={"space-between"}>
                                    <Box>
                                        <Typography variant={"body2"}>Basic Details:</Typography>
                                    </Box>
                                    <Box alignItems="center">
                                        <Button variant={"outlined"} size={"small"}
                                                onClick={() => openEditForm(true)}>Edit</Button>
                                    </Box>
                                </Box>
                                <Box>
                                    <Box>
                                        <Typography variant={"body2"} display={"inline"}>Name :</Typography>
                                        <Typography variant={"subtitle2"}
                                                    display={"inline"}>  {response?.name}</Typography>
                                    </Box>
                                    <Box>
                                        <Typography variant={"body2"} display={"inline"}>Topic :</Typography>
                                        <Typography variant={"subtitle2"}
                                                    display={"inline"}> {response?.topic}</Typography>
                                    </Box>
                                    <Box>
                                        <Typography variant={"body2"} display={"inline"}>Type :</Typography>
                                        <Typography variant={"subtitle2"}
                                                    display={"inline"}>  {response?.type}</Typography>
                                    </Box>

                                    <Box>
                                        <Typography variant={"body2"} display={"inline"}>Subject :</Typography>
                                        <Typography variant={"subtitle2"}
                                                    display={"inline"}>  {response?.subject?.name}</Typography>
                                    </Box>

                                    <Box>
                                        <Typography variant={"body2"} display={"inline"}>Description :</Typography>
                                        <Typography variant={"subtitle2"}
                                                    display={"inline"}>  {response?.description}</Typography>
                                    </Box>
                                    <Box>
                                        <Typography variant={"body2"} display={"inline"}>Total Pages :</Typography>
                                        <Typography variant={"subtitle2"}
                                                    display={"inline"}>  {response?.pages}</Typography>
                                    </Box>
                                    <Box>
                                        <Typography variant={"body2"} display={"inline"}>Date Of Upload :</Typography>
                                        <Typography variant={"subtitle2"}
                                                    display={"inline"}>  {response?.uploadedDate}</Typography>
                                    </Box>
                                </Box>
                            </Card>
                        </Grid>

                        :
                        <Grid item>

                            <MaterialEditForm onEditCancel={() => openEditForm()} onSuccess={() => handleSuccess()}
                                              materialDetail={response}/>

                        </Grid>
                    }

                    <Grid item>
                        <MaterialSwitches materialDetails={response}/>
                    </Grid>

                </Grid>
            </div>
        </>
    );
};

export default MaterialManagePage;