import {makeStyles} from "@material-ui/core";
import './Style.css';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: theme.spacing(3),
        minWidth: 275,
        maxWidth: 500,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
}));


const MainPage =()=>{
    return (
        <>
            <div id='container'>
                <div className="leftmain">
                    <div id="upper">
                        <div><h1>Community driven, free notes from Teachers & Students</h1></div>
                        <div id='search'>
                            <select style={{backgroundColor: "orange", color: 'black', padding: "5px"}}>
                                <option>All courses</option>
                                <option>B.Tech</option>
                            </select>
                            <input type="text" placeholder="search here.." style={{marginLeft: "5px", padding: "5px"}}/>
                            <SearchIcon style={{alignSelf: "center", marginLeft: "10px"}}/>
                        </div>
                    </div>
                    <div id="leftmiddle">
                        <div><h2>Educate</h2></div>
                        <div><h2>Engage</h2></div>
                        <div><h2>Revise</h2></div>
                        <div><h2>Review</h2></div>
                    </div>
                    <div id="leftlast">
                        <div>
                            <img src="images/two.png" style={{marginTop: 40}}/>
                        </div>
                        <div id="lastlast">
                            <div><img src="images/oneicn.png"/><h3>70000 Teachers</h3></div>
                            <div><img src="images/twoicn.png"/><h3>1740000 Users</h3></div>
                            <div><img src="images/threeicn.png"/><h3>9000000 Pages</h3></div>
                            <div><img src="images/fouricn.png"/><h3>5100000 Questions</h3></div>
                        </div>
                    </div>

                </div>
                <div>
                    <img src="images/rightsideimg.png" style={{width: 500, height: 550}}/></div>
            </div>
        </>
    );
}

export default MainPage;